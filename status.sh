#!/bin/bash 
json=$(curl -sb -H http://dati.lazio.it/catalog/it/api/3/action/package_show?id=pronto-soccorso-accessi-in-tempo-reale)
url="http://dati.lazio.it"$(echo $json | egrep -o /catalog/[A-Za-z0-9/-]*.csv);
IFS=
data=$(curl -sb -H $url | tail -n +2 | sort -t";" -k2 );

if [ -z $1 ];
then
    echo -e 'You must provide a code!\n';
    echo -e 'CODE\tOSPEDALE';
    echo -e $data | cut -d";" -f 1,2 --output-delimiter="$(echo -e '\t')";
    exit 1;
fi;

row=$(echo $data | grep "$1[ \;A-Za-z0-9/:_\-]*");

if [ -z $row ];
then
    echo -e 'Code not valid';
    exit 1;
fi;

echo $(echo $row | cut -d";" -f2);
echo -e 'IN ATTESA:';
echo -e '\tBianco:\t'$(echo $row | cut -d";" -f11);
echo -e '\tVerde:\t'$(echo $row | cut -d";" -f10);
echo -e '\tGiallo:\t'$(echo $row | cut -d";" -f9);
echo -e '\tRosso:\t'$(echo $row | cut -d";" -f8);
echo -e '\tNS:\t'$(echo $row | cut -d";" -f12);
echo -e '\tTotale:\t'$(echo $row | cut -d";" -f13);

echo -e 'IN TRATTAMENTO:';
echo -e '\tBianco:\t'$(echo $row | cut -d";" -f18);
echo -e '\tVerde:\t'$(echo $row | cut -d";" -f17);
echo -e '\tGiallo:\t'$(echo $row | cut -d";" -f16);
echo -e '\tRosso:\t'$(echo $row | cut -d";" -f15);
echo -e '\tNS.:\t'$(echo $row | cut -d";" -f19);
echo -e '\tTotale:\t'$(echo $row | cut -d";" -f20);

echo -e 'IN OSSERVAZIONE:';
echo -e '\tBianco:\t'$(echo $row | cut -d";" -f25);
echo -e '\tVerde:\t'$(echo $row | cut -d";" -f24);
echo -e '\tGiallo:\t'$(echo $row | cut -d";" -f23);
echo -e '\tRosso:\t'$(echo $row | cut -d";" -f22);
echo -e '\tNS:\t'$(echo $row | cut -d";" -f26);
echo -e '\tTotale:\t'$(echo $row | cut -d";" -f27);

exit 0;